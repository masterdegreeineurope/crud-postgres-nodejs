const { Router } = require('express')
const router = Router()

router.use('/job', require('./jobs'))
router.use('/employer', require('./employers'))

module.exports = router